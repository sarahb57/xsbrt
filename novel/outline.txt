Title: DNF

Part I: The Ascent

1. Prologue: A Promising Start
   - Introduction of Chris Nelloms, a young and talented athlete from Dayton, Ohio, with a bright future
   - Background on his family, upbringing, and the challenges faced in Dayton's urban landscape
   - Historical context of urban decline and socio-economic challenges in American cities during this period
   - Description of the physical landscape of public housing in inner-city Dayton, Ohio, including the architecture and design of the housing units and their impact on residents' quality of life
   - Examination of the living conditions inside the public housing units, including overcrowding and lack of maintenance, and their effects on residents' physical and mental health
   - Discussion of the social issues prevalent in public housing communities, such as poverty, drug addiction, and gang violence, and the factors contributing to these issues, including unemployment and lack of access to resources
   - Exploration of the tight-knit communities that formed within public housing neighborhoods and the ways in which residents supported and relied on one another for survival and upward mobility
   - Examination of the challenges faced by children growing up in public housing communities and the creative ways in which they utilized the urban landscape for play and entertainment
   - Analysis of the lasting impact of public housing in inner-city Dayton, Ohio, on the city's development and growth, and the lessons learned from this era, including the importance of investing in affordable housing and fostering strong communities.

2. Running Through Welcome Stadium
   - Chris's early experiences and achievements at Welcome Stadium
   - The excitement and optimism of the athletic world embracing him as the next big star

3. From High School to College: A Journey of Success and Friendship
- The intense rivalry between Chris Nelloms and Robert Smith from Euclid High School
- Nelloms' transition from Dayton Dunbar High School to Ohio State University
- Nelloms' remarkable success on the track, including his No. 1 rankings in the nation for the 400, 200, and high hurdles, and being named Ohio State's male athlete of 1993
- Nelloms' thoughts on the keys to success, citing his mother's discipline and his avoidance of drugs and negative influences
- Nelloms' friendship with Dan "Big Daddy" Wilkinson and their shared journey of overcoming obstacles and defying skeptics

4. The Paradox of Pressure
   - Chris's internal struggles with anxiety and self-doubt, despite his impressive accomplishments
   - His unique approach to handling pressure, giving him a sense of control over his fate

Part II: The Fall

5. The Turning Point
- The fateful night of August 2, 1992, when Chris Nelloms was ambushed and shot in the back during a training run in a Dayton park, shattering his clavicle and leaving a scar across half his chest
- The severity of his injuries, his harrowing crawl home, and the long road to recovery
- Chris's determination and perseverance throughout his recovery process, including his refusal to redshirt and his impressive return to competition

6. A Life Rededicated
   - Chris's miraculous recovery and his NCAA 200 title win just seven months later
   - His appreciation for his mother's sacrifices and his determination to make something of himself

Part VII: A Shift in Perspective

7. The Impact of Adversity
   - Chris's survival of the gunshot wound and its impact on his perspective on life
   - How his faith played a role in his miraculous recovery
   - The change in his attitude towards challenges and obstacles, both in sports and in life

8. Overcoming the Past
   - The role of Chris's past experiences in shaping his mindset and approach to life
   - The challenges he faced growing up in a community plagued by drugs and crime
   - How he used his experiences to motivate himself to succeed and to inspire others to do the same

Title: State of Ohio v. Christopher R. Nelloms

Court: Court of Appeals of Ohio, Second District, Montgomery County

Date Published: December 3, 1999

Case Number: C.A. Case No. 17663, T.C. Case No. 98-CR-1590

Parties:
Plaintiff-Appellee: State of Ohio
Defendant-Appellant: Christopher R. Nelloms

Attorneys:
- Cheryl A. Ross, Assistant Prosecuting Attorney, Appellate Division, Atty. Reg. #0066113, representing the Plaintiff-Appellee
- Charles W. Slicer, III, Atty. Reg. #0059927, representing the Defendant-Appellant

Judges: Fain, J., Brogan, J., and Wolff, J.

Opinion:

9. Background
- Nelloms, a track star, was indicted for the rape of Terria Pitts, a girl under the age of 13, in Trotwood, Ohio between October 1995 and August 1997.
- Nelloms was found guilty of Felonious Sexual Penetration and seven counts of Rape. He was acquitted of Attempted Forcible Rape and one count of Rape was nolled.
- Nelloms appealed his conviction and sentence, citing lack of subject matter jurisdiction, juror misconduct, and ineffective assistance of counsel.

10. Issues
- The trial court lacked subject matter jurisdiction over four counts of Rape.
- Nelloms claimed that juror misconduct and ineffective assistance of counsel affected the outcome of the trial.

11. Analysis
- Lack of Subject Matter Jurisdiction: The Court found that the trial court lacked jurisdiction over four counts of Rape, but upheld the remaining counts.
- Juror Misconduct: Nelloms claimed that a juror was biased against him, but the trial court found the juror to be impartial. The Court upheld the trial court's decision.
- Ineffective Assistance of Counsel: The Court found that Nelloms failed to demonstrate that his trial counsel's performance fell below an objective standard of reasonableness.

12. Conclusion
- Nelloms' conviction was upheld for Felonious Sexual Penetration and three counts of Rape.
- Nelloms' claims of juror misconduct and ineffective assistance of counsel were dismissed.
- The trial court was instructed to dismiss the four counts of Rape for lack of subject matter jurisdiction.
- Nelloms faced a mandatory life sentence but was eligible for parole.

13. Additionally:
- Friends and family of Nelloms maintained his innocence and expressed disbelief at the verdict.
- Randy Faison, a friend of Nelloms, argued that the case was not thoroughly investigated.
- Angela Frydman, Montgomery County assistant prosecutor, acknowledged the sadness of the situation but believed the guilty verdict was the only true outcome based on the facts and evidence.


Part III: Life in Prison
14. Introduction to Chris Nelloms' Life in Prison
   - The challenges and limitations of life in prison

15. A Typical Day for Chris Nelloms
   - Morning: Reflection, prayer, and Bible study
   - Daytime: Work, recreation, and scheduled counts
   - Evening: Church, bonding with cellmate, and continued spiritual pursuits

16. Nighttime Reflections and Sleep
   - Rereading passages from the Bible, praying for strength and guidance
   - Thoughts about the past, present, and future

17. Waking Up and Facing Reality
   - Moments of disbelief and difficulty accepting the situation
   - Focus on the present moment and making the most of each day

Part IV: Overcoming Challenges Through Faith
18. The Impact of Faith and Past Experiences
   - Belief in a purpose and preparation for life outside prison
   - Drawing strength from surviving a near-death experience

19. Emotional and Mental Freedom
   - Emphasizing inner freedom despite physical confinement
   - Staying positive and hopeful for the future

Part V: The Passage of Time and Its Effects
20. Perception of Time in Prison
   - The feeling that time moves quickly, yet also the realization of lost moments
   - The emotional impact of seeing his daughter grow up from a distance
 - The prospect of parole in December 2038

21. Communication with Family and Friends
   - Regular communication with close family members, agent, and mentors
   - Occasional letters from fans, acquaintances, and friends from the past
   -The challenges of staying connected and maintaining relationships while incarcerated

22. Drifting Apart from College Friends
   - The absence of communication with college friends
   - Appreciating the memories and friendships shared during that time

Part VI: Life in Prison and Its Challenges
23. Inmate Awareness of Nelloms' Past
   - Some inmates know about his track career, while others do not
   - Learning from life experiences and focusing on personal growth

24. What Nelloms Misses the Most
   - Missing his children and family more than anything else
   - The loss of his career and the opportunities it provided
   - A longing for freedom, simple joys, and making a positive impact on the world

25. The Worst Aspects of Prison Life
   - The feeling of isolation and disconnection from the outside world
   - Coping with the monotony and limited freedoms

Part VII: Staying Active and Preparing for the Future
26. Track Competitions and Staying Active
   - Annual Fourth of July events, including various races
   - Nelloms being unable to compete, but still coaching and training other inmates

27. Maintaining Physical Fitness and Staying Involved in Sports
   - Nelloms as a mentor and coach for other inmates
   - Staying in shape to adjust to life outside of prison

Part VIII: Fighting for Justice and Maintaining Hope
28. Chris Nelloms' Assertion of Innocence
   - Insistence on innocence and commitment to fighting for the truth
   - Reflections on the plea deal and the decision not to accept it

29. What Nelloms Wants People to Know
   - An innocent man who is fighting for his freedom
   - A father, son, and brother who is loved and missed by his family
   - A person defined by resilience and determination, not by the circumstances

30. A Moment of Anger and Pain
   - Terria Pitts, on her apartment complex's lawn, trying to calm herself
   - Concern for her children, Paul and Triniti, not seeing her upset
   - The anger and frustration caused by Chris Nelloms' denials

31. A Haunting Past
   - Nelloms, a former Dunbar High track star, convicted of sexual crimes involving Pitts
   - The impact of the crime on Terria, a young girl at the time

32. The Struggle for Truth and Justice
   - Terria's response to Nelloms' claims of innocence
   - Her determination to let the public know the truth about her father
   - Emphasizing the evidence that led to Nelloms' conviction

33. Speaking Out With Courage
   - Terria's emotional testimony, sharing her side of the story
   - The supportive reactions of those in attendance
   - The importance of her voice in the ongoing fight for justice and healing

34. The Pain of Reliving Trauma
   - Terria's distress from reading newspaper articles about Nelloms
   - The emotional turmoil caused by Nelloms' continued denial of the truth

35. A Life Forever Changed
   - The lasting impact of Nelloms' actions on Terria's life
   - Terria's decision to contact the Dayton Daily News and share her story

36. The Power of Speaking Out
   - The significance of Terria, now an adult, choosing to be identified as a survivor
   - The importance of her voice in the ongoing fight for justice and healing

37. The Road Ahead
   - Terria's resilience and determination to move forward
   - The ongoing challenge to find closure and healing
   - The reminder that talent does not excuse harmful actions


38. Epilogue: The Lasting Impact of Faith and Perseverance
   - The ongoing story of Chris Nelloms' journey through prison
   - The importance of hope, faith, and resilience in the face of adversity
   - The possibility of redemption and the chance to rebuild a life after release