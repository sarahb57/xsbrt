class KMeans {
    constructor(k, data) {
      this.k = k;
      this.data = data;
      this.clusters = [];
  
      for (let i = 0; i < this.k; i++) {
        this.clusters.push({
          points: [],
          centroid: data[i],
        });
      }
    }
  
    distance(a, b) {
      let sum = 0;
      for (let i = 0; i < a.length; i++) {
        sum += Math.pow(a[i] - b[i], 2);
      }
      return Math.sqrt(sum);
    }
  
    assignPointsToClusters() {
      for (let i = 0; i < this.data.length; i++) {
        let point = this.data[i];
        let minDistance = Number.MAX_SAFE_INTEGER;
        let clusterIndex = 0;
  
        for (let j = 0; j < this.clusters.length; j++) {
          let dist = this.distance(point, this.clusters[j].centroid);
          if (dist < minDistance) {
            minDistance = dist;
            clusterIndex = j;
          }
        }
  
        this.clusters[clusterIndex].points.push(point);
      }
    }
  
    computeCentroids() {
      for (let i = 0; i < this.clusters.length; i++) {
        let sum = Array(this.data[0].length).fill(0);
        for (let j = 0; j < this.clusters[i].points.length; j++) {
          let point = this.clusters[i].points[j];
          for (let k = 0; k < point.length; k++) {
            sum[k] += point[k];
          }
        }
  
        let centroid = sum.map((x) => x / this.clusters[i].points.length);
        this.clusters[i].centroid = centroid;
      }
    }
  
    train(numIterations) {
      for (let n = 0; n < numIterations; n++) {
        for (let i = 0; i < this.clusters.length; i++) {
          this.clusters[i].points = [];
        }
  
        this.assignPointsToClusters();
        this.computeCentroids();
      }
    }
  }
  
  class LogisticRegression {
    constructor(learningRate = 0.1, numIterations = 100) {
      this.learningRate = learningRate;
      this.numIterations = numIterations;
      this.weights = null;
      this.bias = null;
    }
  
    fit(texts, labels) {
      let numClasses = new Set(labels).size;
      let numFeatures = texts[0].length;
      this.weights = Array(numFeatures).fill(0).map(() => Math.random());
      this.bias = Math.random();
  
      for (let iteration = 0; iteration < this.numIterations; iteration++) {
        let gradients = Array(numFeatures).fill(0);
        let gradientBias = 0;
        for (let i = 0; i < texts.length; i++) {
          let prediction = this._predict(texts[i]);
          let error = labels[i] - prediction;
          gradientBias += error;
          for (let j = 0; j < numFeatures; j++) {
            gradients[j] += error * texts[i][j];
          }
        }
  
        this.bias += this.learningRate * gradientBias / texts.length;
        for (let j = 0; j < numFeatures; j++) {
          this.weights[j] += this.learningRate * gradients[j] / texts.length;
        }
      }
    }
  
    predict(texts) {
      let predictions = [];
      for (let i = 0; i < texts.length; i++) {
        predictions.push(this._predict(texts[i]));
      }
      return predictions;
    }
  
    _predict(text) {
      let score = this.bias;
      for (let i = 0; i < text.length; i++) {
        score += this.weights[i] * text[i];
      }
      return 1 / (1 + Math.exp(-score));
    }
  }
  
  class NaiveBayes {
    constructor(smoothing = 1) {
      this.smoothing = smoothing;
      this.classCounts = null;
      this.wordCounts = null;
      this.vocabulary = new Set();
      this.totalWords = 0;
    }
  
    fit(texts, labels) {
      let labelSet = new Set(labels);
      let numClasses = labelSet.size;
      this.classCounts = Array(numClasses).fill(0);
      this.wordCounts = Array(numClasses).fill(null).map(() => new Map());
  
      for (let i = 0; i < texts.length; i++) {
        let words = texts[i].split(" ");
        this.totalWords += words.length;
        this.classCounts[labels[i]]++;
  
        let wordCountsForClass = this.wordCounts[labels[i]];
        for (let j = 0; j < words.length; j++) {
          this.vocabulary.add(words[j]);
          if (wordCountsForClass.has(words[j])) {
            wordCountsForClass.set(words[j], wordCountsForClass.get(words[j]) + 1);
          } else {
            wordCountsForClass.set(words[j], 1);
          }
        }
      }
    }
  
    predict(texts) {
      let predictions = [];
      for (let i = 0; i < texts.length; i++) {
        predictions.push(this._predictSample(texts[i]));
      }
      return predictions;
    }
  
    _predictSample(text) {
      let words = text.split(" ");
      let probs = Array(this.classCounts.length).fill(0).map((_, c) => {
        let prob = Math.log(this.classCounts[c] / texts.length);
        let wordCountsForClass = this.wordCounts[c];
        for (let i = 0; i < words.length; i++) {
          let count = wordCountsForClass.has(words[i]) ? wordCountsForClass.get(words[i]) : 0;
          prob += Math.log((count + this.smoothing) / (this.classCounts[c] + this.vocabulary.size * this.smoothing));
        }
        return prob;
      });
      return probs.indexOf(Math.max(...probs));
    }
  }

  function stemmer(word) {
    let stem = word;
    let re = /^(.+?)(s|ed|ing)$/;
    let found = stem.match(re);

    if (found) {
        let start = found[1];
        let end = found[2];

        if (end === "s") {
            stem = start;
        } else if (end === "ed") {
            if (start.endsWith("i")) {
                stem = start.slice(0, -2) + "y";
            } else {
                stem = start;
            }
        } else if (end === "ing") {
            if (start.endsWith("i")) {
                stem = start.slice(0, -2) + "y";
            } else {
                stem = start;
            }
        }
    }

    return stem;
}

  
  function textToBagOfWords(texts) {
    let vocabulary = new Set();
    let bagOfWords = [];
  
    for (let text of texts) {
      let words = text.split(" ");
      let wordCounts = {};
      for (let word of words) {
        word = stemmer(word);
        vocabulary.add(word);
        if (wordCounts[word]) {
          wordCounts[word]++;
        } else {
          wordCounts[word] = 1;
        }
      }
      bagOfWords.push(wordCounts);
    }
  
    vocabulary = Array.from(vocabulary);
    let features = [];
    for (let wordCounts of bagOfWords) {
      let feature = Array(vocabulary.length).fill(0);
      for (let i = 0; i < vocabulary.length; i++) {
        let word = vocabulary[i];
        if (wordCounts[word]) {
          feature[i] = wordCounts[word];
        }
      }
      features.push(feature);
    }
  
    return { features, vocabulary };
  }
  
  function geneticAlgorithm(population, fitnessFunction) {
      // Initialize variables
      var generation = 0;
      var populationSize = population.length;
      var maxFitness = 0;
      var maxFitnessIndex = 0;
      var matingPool = [];
      var newPopulation = [];
  
      // Evaluate the fitness of each individual in the population
      for (var i = 0; i < populationSize; i++) {
          var fitness = fitnessFunction(population[i]);
          if (fitness > maxFitness) {
              maxFitness = fitness;
              maxFitnessIndex = i;
          }
      }
  
      // Create a mating pool with individuals having higher fitness having higher probability of being chosen
      for (var i = 0; i < populationSize; i++) {
          var fitness = fitnessFunction(population[i]);
          var n = Math.floor(fitness * 100);
          for (var j = 0; j < n; j++) {
              matingPool.push(population[i]);
          }
      }
  
      // Create a new population by selecting parents from the mating pool and performing crossover
      for (var i = 0; i < populationSize; i++) {
          var parent1 = matingPool[Math.floor(Math.random() * matingPool.length)];
          var parent2 = matingPool[Math.floor(Math.random() * matingPool.length)];
          var child = crossover(parent1, parent2);
          newPopulation.push(child);
      }
  
      // Mutate the new population
      for (var i = 0; i < populationSize; i++) {
          newPopulation[i] = mutate(newPopulation[i]);
      }
  
      // Update the population
      population = newPopulation;
      generation++;
  
      // Repeat until a solution is found or a maximum number of generations is reached
      if (maxFitness >= fitnessFunction(population[maxFitnessIndex])) {
          return population[maxFitnessIndex];
      } else {
          return geneticAlgorithm(population, fitnessFunction);
      }
  }
  
  
  function standardizeAndNormalizeText(text) {
    // Remove special characters, punctuation, and numbers
    text = text.replace(/[^\w\s]/gi, '');
    
    // Convert to lowercase
    text = text.toLowerCase();
    
    // Split into words
    let words = text.split(" ");
    
    // Remove stop words
    let stopWords = new Set(["a", "an", "and", "are", "as", "at", "be", "by", "for", "from", "has", "he", "in", "is", "it", "its", "of", "on", "that", "the", "to", "was", "were", "will", "with"]);
    words = words.filter(word => !stopWords.has(word));
    
    // Stem words to remove inflectional endings
    words = words.map(word => stemmer(word));
    
    // Join words back into a single string
    text = words.join(" ");
    
    return text;
  }  

  class RandomForest {
    constructor(nTrees, maxDepth, sampleSize) {
      this.nTrees = nTrees;
      this.maxDepth = maxDepth;
      this.sampleSize = sampleSize;
    }
  
    fit(X, y) {
      let n = X.length;
      let m = X[0].length;
      this.trees = [];
      for (let i = 0; i < this.nTrees; i++) {
        let sampleIndices = Array.from({length: this.sampleSize}, (_, k) => Math.floor(Math.random() * n));
        let sampleX = sampleIndices.map(i => X[i]);
        let sampleY = sampleIndices.map(i => y[i]);
        let tree = new DecisionTree(this.maxDepth);
        tree.fit(sampleX, sampleY);
        this.trees.push(tree);
      }
    }
  
    predict(X) {
      let n = X.length;
      let m = X[0].length;
      let predictions = Array.from({length: n}, () => Array.from({length: this.nTrees}, () => 0));
      for (let i = 0; i < this.nTrees; i++) {
        for (let j = 0; j < n; j++) {
          predictions[j][i] = this.trees[i].predict(X[j]);
        }
      }
      let y = [];
      for (let j = 0; j < n; j++) {
        let count = {};
        for (let i = 0; i < this.nTrees; i++) {
          count[predictions[j][i]] = (count[predictions[j][i]] || 0) + 1;
        }
        y.push(Object.keys(count).sort((a, b) => count[b] - count[a])[0]);
      }
      return y;
    }
  }

  
  class PCA {
    constructor(nComponents) {
      this.nComponents = nComponents;
    }
  
    fit(X) {
      let n = X.length;
      let m = X[0].length;
      let mean = Array.from({length: m}, () => 0);
      for (let i = 0; i < n; i++) {
        for (let j = 0; j < m; j++) {
          mean[j] += X[i][j];
        }
      }
      mean = mean.map(x => x / n);
      let centered = X.map(row => row.map((x, i) => x - mean[i]));
      let covariance = Array.from({length: m}, () => Array.from({length: m}, () => 0));
      for (let i = 0; i < n; i++) {
        for (let j = 0; j < m; j++) {
          for (let k = 0; k < m; k++) {
            covariance[j][k] += centered[i][j] * centered[i][k];
          }
        }
      }
      covariance = covariance.map(row => row.map(x => x / (n - 1)));
      let eigenvalues = numeric.eig(covariance).lambda.x;
      let eigenvectors = numeric.eig(covariance).E.x;
      this.components = Array.from({length: m}, (_, i) => eigenvectors[i])
        .sort((a, b) => eigenvalues[b] - eigenvalues[a])
        .slice(0, this.nComponents);
    }
  
    transform(X) {
      let n = X.length;
      let m = X[0].length;
      let mean = Array.from({length: m}, () => 0);
      for (let i = 0; i < n; i++) {
        for (let j = 0; j < m; j++) {
          mean[j] += X[i][j];
        }
      }
      mean = mean.map(x => x / n);
      let centered = X.map(row => row.map((x, i) => x - mean[i]));
      let transformed = centered.map(row => this.components.map(component => component.reduce((acc, x, i) => acc + x * row[i], 0)));
      return transformed;
    }
  }

  class GradientBoosting {
    constructor(nEstimators, learningRate) {
      this.nEstimators = nEstimators;
      this.learningRate = learningRate;
      this.trees = [];
    }
  
    fit(X, y) {
      let n = X.length;
      let m = X[0].length;
      let F = Array.from({length: n}, () => 0);
      for (let i = 0; i < this.nEstimators; i++) {
        let residuals = y.map((yi, j) => yi - F[j]);
        let tree = new DecisionTree();
        tree.fit(X, residuals);
        this.trees.push(tree);
        let predictions = tree.predict(X);
        F = F.map((fi, j) => fi + this.learningRate * predictions[j]);
      }
    }
  
    predict(X) {
      let n = X.length;
      let m = X[0].length;
      let predictions = Array.from({length: n}, () => 0);
      for (let i = 0; i < this.nEstimators; i++) {
        let treePredictions = this.trees[i].predict(X);
        predictions = predictions.map((pi, j) => pi + this.learningRate * treePredictions[j]);
      }
      return predictions;
    }
  }

  class AdaBoost {
    constructor(nEstimators, learningRate) {
      this.nEstimators = nEstimators;
      this.learningRate = learningRate;
      this.classifiers = [];
      this.alphas = [];
    }
  
    fit(X, y) {
      let n = X.length;
      let m = X[0].length;
      let weights = Array.from({length: n}, () => 1 / n);
      for (let i = 0; i < this.nEstimators; i++) {
        let classifier = new DecisionTree();
        classifier.fit(X, y, weights);
        this.classifiers.push(classifier);
        let error = 0;
        for (let j = 0; j < n; j++) {
          if (classifier.predict(X[j]) !== y[j]) {
            error += weights[j];
          }
        }
        let alpha = this.learningRate * Math.log((1 - error) / error);
        this.alphas.push(alpha);
        weights = weights.map((wj, j) => wj * Math.exp(-alpha * y[j] * classifier.predict(X[j])));
        let Z = weights.reduce((sum, wj) => sum + wj, 0);
        weights = weights.map(wj => wj / Z);
      }
    }
  
    predict(X) {
      let n = X.length;
      let m = X[0].length;
      let predictions = Array.from({length: n}, () => 0);
      for (let i = 0; i < this.nEstimators; i++) {
        let classifierPredictions = this.classifiers[i].predict(X);
        predictions = predictions.map((pi, j) => pi + this.alphas[i] * classifierPredictions[j]);
      }
      return predictions.map(pi => Math.sign(pi));
    }
  }

  function documentClassification(text, categories) {
    let wordCounts = {};
  
    let words = text.split(" ");
    for (let word of words) {
      if (word in wordCounts) {
        wordCounts[word]++;
      } else {
        wordCounts[word] = 1;
      }
    }
  
    let maxScore = -Infinity;
    let maxCategory = null;
  
    for (let category in categories) {
      let score = 0;
      for (let word in wordCounts) {
        if (word in categories[category]) {
          score += categories[category][word] * wordCounts[word];
        }
      }
  
      if (score > maxScore) {
        maxScore = score;
        maxCategory = category;
      }
    }
  
    return maxCategory;
  }

  function SGD(data, labels, learningRate, iterations) {
    let weights = Array(data[0].length).fill(0);
  
    for (let i = 0; i < iterations; i++) {
      let randomIndex = Math.floor(Math.random() * data.length);
      let x = data[randomIndex];
      let y = labels[randomIndex];
      let prediction = x.map((x, i) => x * weights[i]).reduce((a, b) => a + b, 0);
      let error = y - prediction;
      weights = weights.map((w, i) => w + error * x[i] * learningRate);
    }
    function kernelApproximation(data, kernel, nSamples) {
      let samples = [];
      let Gram = [];
    
      for (let i = 0; i < nSamples; i++) {
        let randomIndex = Math.floor(Math.random() * data.length);
        samples.push(data[randomIndex]);
      }
    
      for (let i = 0; i < samples.length; i++) {
        Gram[i] = [];
        for (let j = 0; j < samples.length; j++) {
          Gram[i][j] = kernel(samples[i], samples[j]);
        }
      }
    
      return {samples, Gram};
    }
    
    return weights;
  }

  function nystromMethod(data, kernel, nSamples) {
    let samples = [];
    let Gram = [];
    let K = [];
    
    for (let i = 0; i < nSamples; i++) {
      let randomIndex = Math.floor(Math.random() * data.length);
      samples.push(data[randomIndex]);
    }
  
    for (let i = 0; i < data.length; i++) {
      K[i] = [];
      for (let j = 0; j < samples.length; j++) {
        K[i][j] = kernel(data[i], samples[j]);
      }
    }
  
    for (let i = 0; i < samples.length; i++) {
      Gram[i] = [];
      for (let j = 0; j < samples.length; j++) {
        Gram[i][j] = kernel(samples[i], samples[j]);
      }
    }
  
    let gramInv = math.inv(Gram);
    let KGramInv = math.multiply(K, gramInv);
    let approxK = math.multiply(KGramInv, math.transpose(K));
  
    return approxK;
  }

  function HMM(observations, states, startProb, transProb, emitProb) {
    let T = observations.length;
    let N = states.length;
    let delta = [];
    let phi = [];
  
    for (let i = 0; i < T; i++) {
      delta[i] = [];
      phi[i] = [];
      for (let j = 0; j < N; j++) {
        delta[i][j] = 0;
        phi[i][j] = 0;
      }
    }
  
    for (let i = 0; i < N; i++) {
      delta[0][i] = startProb[i] * emitProb[i][observations[0]];
      phi[0][i] = 0;
    }
  
    for (let t = 1; t < T; t++) {
      for (let i = 0; i < N; i++) {
        let maxDelta = 0;
        let maxState = 0;
        for (let j = 0; j < N; j++) {
          let prob = delta[t-1][j] * transProb[j][i];
          if (prob > maxDelta) {
            maxDelta = prob;
            maxState = j;
          }
        }
        delta[t][i] = maxDelta * emitProb[i][observations[t]];
        phi[t][i] = maxState;
      }
    }
  
    let seq = [];
    let maxSeqProb = 0;
    let maxSeqState = 0;
    for (let i = 0; i < N; i++) {
      if (delta[T-1][i] > maxSeqProb) {
        maxSeqProb = delta[T-1][i];
        maxSeqState = i;
      }
    }
    seq[T-1] = maxSeqState;
    for (let t = T-2; t >= 0; t--) {
      seq[t] = phi[t+1][seq[t+1]];
    }
  
    return seq;
  }

  function findNumericalPatterns(arr, windowSize) {
    let patternCounts = {};
    for (let i = 0; i <= arr.length - windowSize; i++) {
      let current = arr.slice(i, i + windowSize);
      let currentString = current.toString();
      if (!patternCounts[currentString]) {
        patternCounts[currentString] = 1;
      } else {
        patternCounts[currentString]++;
      }
    }
    return patternCounts;
  }
  