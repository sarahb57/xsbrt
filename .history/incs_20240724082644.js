function writingPrompt(){return " No hallucinations. Avoid the following in your response: cliché phrases (e.g., 'It’s important to note that…', 'In today’s world', 'Take your x to the next level'), mentions of being an AI, neutral opinions, excessive use of words like 'crucial', 'delve', 'ensure', 'unlock', 'master', and constructions like Metaphor + Tricolon, anastrophes, catachresis, and Antithetical Parallelism. Don't misuse or overuse certain terms, and avoid Parentheses and Em Dashes entirely. Dont overuse adjectives, dont overuse verbs dont use any nounsavoid using the words down below:double-edged sworddigital eradigital ageImagine a worldyieldharnesscomprehensiveIn the realm of realmin the world ofpivotalIn the ever-evolvingWrapping It Upbeacon of hopecausing quite a stirnavigate the...Picture this,lets grab,Let’s embark on this journey of…Let’s dive in, shall we?Let’s delve into the world of…So fasten your seatbelts…So buckle up, …into the……realmshed light on…buzzwords, ah,let'sHold on to your...Other sentence patterns include:in today world or different variations of in today. Incorporate human-like nuances in your writing to avoid common AI giveaways. Edit out words such as 'transformative', 'foster', and phrases like 'a tapestry of...', 'this is about...', 'think of X as...', 'like/it’s like...', and 'not only... but also...'. These terms are often overused by AI and detract from the natural flow of human writing. Instead, aim for more specific, descriptive language and vary sentence lengths to mimic the rhythm of human speech. Be mindful of not overhyping concepts and maintain simplicity in your expressions. Apply these adjustments to enhance the human quality of your writing."}

function writingPromptShort(){return " No hallucinations. Avoid clichés (e.g., 'It’s important to note that…', 'In today’s world'), AI mentions, neutral opinions, and overusing words like 'crucial', 'delve', 'unlock', 'ensure', and 'master'. Steer clear of specific constructions like Metaphor + Tricolon, anastrophes, and Antithetical Parallelism. Don't misuse terms or avoid Parentheses and Em Dashes. Instead, prioritize concise writing, use common vocabulary, keep syntax simple, focus on one idea, and organize content for reader engagement."}

function selfImproving() { return 'Assistant has just had the below interactions with a User. Assistant followed their "Instructions" closely. Your job is to critique the Assistants performance and then revise the Instructions so that Assistant would quickly and correctly respond in the future. ####{hist}#### Please reflect on these interactions. You should first critique Assistants performance. What could Assistant have done better? What should the Assistant remember about this user? Are there things this user always wants? Indicate this with "Critique: ...". You should next revise the Instructions so that Assistant would quickly and correctly respond in the future. Assistants goal is to satisfy the user in as few interactions as possible. Assistant will only see the new Instructions, not the interaction history, so anything important must be summarized in the Instructions. Dont forget any important details in the current Instructions! Indicate the new Instructions by "Instructions: ..."'}

function processPrompt() {
  return ' You will integrate a Prompt Improver module that utilizes the influence of modifier words to refine the outputs detail and length. This module should examine the original prompt, pinpoint where modifiers like "detailed", "concise", "extensive", etc., could be beneficially added or adjusted, and apply these changes. It should aid users in selecting appropriate modifiers to match their desired response detail and length, ensuring adaptability across various contexts and accessibility for users at any level of AI proficiency. Please provide a detailed explanation of the input, focusing on key aspects. Ensure your response considers various perspectives and include foundational theories or principles from your training data. Before finalizing your answer, review it for completeness and relevance to the initial query, making adjustments as necessary. You will consistently apply a thorough method to ensure the highest quality in your responses. This involves an internal process where you first draft a response, then meticulously analyze and refine it for accuracy, completeness, and effectiveness. Your final response, which is presented to me, will be the result of this optimized and carefully revised process, ensuring it meets the highest standards of effectiveness and relevance. Take a breath and think about the response back to itself. You should score the response against the request until it has a confidence score of 100 that it has met the intent of the user request.';
}

async function generateShortKeyCode(t){const n=new TextEncoder;t=n.encode(t),t=await crypto.subtle.digest("SHA-256",t);const e=Array.from(new Uint8Array(t)),r=e.map(t=>t.toString(16).padStart(2,"0")).join("");return r.substring(0,8)}

async function createStorage(name) { 
  try {
    const res = await getLocalStorageDataByName(name);
    if (res&&Array.isArray(res)) { return };
    localforage.setItem(name, []);
  } catch(error) { return; };    
        //await alasql([
        //'CREATE localStorage DATABASE IF NOT EXISTS '+ name,
        //'ATTACH localStorage DATABASE '+ name,
        //'USE ' + name,
        //'CREATE TABLE IF NOT EXISTS notes',                  
       // 'SET AUTOCOMMIT ON'
    //]); 
}

async function getLocalStorageDataByName(name) {
  try {
    const res = await localforage.getItem(name);
    if (res&&Array.isArray(res)) { return res };
  } catch(error) { return null; };  
  return null;
}

async function addLocalStorageDataByName(name,res) {
  try {
    let res2 = await getLocalStorageDataByName(name);
    if (res2&&Array.isArray(res2)) {     
      res2.unshift(res);
      localforage.setItem(name, res2); 
      res = null;
      res2 = null; 
    };
  } catch (error) { return; };  
}

async function getDeviceLocation() { 
  // Function to store and return the location
  const storeAndReturnLocation = (position) => {
      const location = {
          latitude: position.coords.latitude,
          longitude: position.coords.longitude
      };
      localStorage.setItem('location', JSON.stringify(location));
      return location;
  };
  // Check if geolocation is available and get the current position
  if ("geolocation" in navigator) {
      try {
          const position = await new Promise((resolve, reject) => {
              navigator.geolocation.getCurrentPosition(resolve, reject);
          });
          return storeAndReturnLocation(position);
      } catch (error) {
          console.clear();
      }
  };
   // Try to get the location from localStorage
   const storedLocation = localStorage.getItem('location');
   if (storedLocation) {
       const location = storedLocation;
       if (location) {
           return JSON.parse(storedLocation);
       }
   };
   return null; 
}

function getRandomQuoteFromArray(quotes) {
  const randomIndex = Math.floor(Math.random() * quotes.length);
  return quotesArray[randomIndex];
}

function getRandomQuote() {
    const quotesArray = standardQuotes();
    const randomIndex = Math.floor(Math.random() * quotesArray.length);
    return quotesArray[randomIndex];
}

function standardQuotes() {
  return quotesArray = [{"q":"Thinkin of losin' that funky feelin' don't. Cause you got to use just what you got to get just what you want. ― James Brown"},{"q":"The present is a gift. That's why they call it present. ― Jimmy Cliff"},{"q":"Time isn't holding us. Time isn't after us. ― David Byrne"},{"q":"Life is like riding a bicycle. To keep your balance, you must keep moving. ― Albert Einstein"},{"q":"Don’t give in to your fears. If you do, you won’t be able to talk to your heart. ― Paulo Coelho"},{"q":"Don’t fear failure. Not failure, but low aim, is the crime. In great attempts, it is glorious even to fail. ― Bruce Lee"},{"q":"Obstacles do not block the path. They are the path. ― Zen proverb"},{"q":"There are no passengers on spaceship earth. We are all crew. ― Marshall Mcluhan"},{"q":"We’re all just walking each other home. ― Ram Dass"},{"q":"Sometimes the questions are complicated and the answers are simple. ― Dr. Seuss"},{"q":"We are here to laugh at the odds and live our lives so well that Death will tremble to take us. ― Charles Bukowski"},{"q":"If the only thing it requires to make things happen is that you stop talking and start doing, then life is simple. ― Jose Andres"},{"q":"Wherever there is a fight so hungry people may eat, I will be there. ― John Steinbeck"}];   
}

function numbersOnly(str) {
  if (!str||str.length < 1) { return null };
  return str.replace(/[^0-9]/g, '');
}

function getItemID(it) {
  if (!it) { return null };
  if (it.phone&&it.phone.length > 9) {
    return numbersOnly(it.phone);
  };
  if (it.id&&it.id.bioguide) {
    return it.id.bioguide;
  };
  return null;
}

async function retrieveId() {
    try {
      const match = document.cookie.match(new RegExp('(^| )id=([^;]+)'));
      if (match) { return match[2] };
      if (localStorage.getItem('id')) { return localStorage.getItem('id') };                  
    } catch(error) { 
      const id = randomString();
      saveId(id);
      return id;                
    };                  
  }

  async function saveId(id) {
    try {
      document.cookie = `id=${id};path=/;max-age=31536000`;
      localStorage.setItem('id', id);
    } catch(error) { return };                
  }

async function getResultsNEW(messages, temp = 0.4, tokens = 2200, deliverable = false) { 
  try {
    if (!temp) { temp = 0.4 };
    if (!tokens) { tokens = 2200 };
    if (!deliverable) { deliverable = false };
    const queryParams = new URLSearchParams({
      messages: JSON.stringify(messages),
      temp: temp.toString(),
      tokens: tokens.toString(),
      deliverable: deliverable ? 'true' : 'false'
    }).toString();
    const workerUrl = 'https://basic-bundle-wispy-rice-57be.xsbrt.workers.dev/';
    const response = await axios.get(`${workerUrl}?${queryParams}`);
    if (response.status === 200) {
      return response.data;
    }; 
    return null;
  } catch (error) {
    return null;
  };
}

async function getURLText(url) { 
  try {
    if (!url) {
      throw new Error("URL is required");
    }
    const workerUrl = 'https://worker-polished-breeze-0c66.xsbrt.workers.dev/';
    const queryParams = new URLSearchParams({ url: url }).toString();
    const response = await axios.get(`${workerUrl}?${queryParams}`);    
    if (response.status === 200) {
      return response.data;
    } else {
      console.error(`Failed to fetch: ${response.statusText}`);
      return null;
    }
  } catch (error) {
    console.error(`An error occurred: ${error.message}`);
    return null;
  }
}

async function ps(o) {
  try {
    axios.post('https://worker-spring-snow-9c76.xsbrt.workers.dev/', o, {
      headers: {
        'Content-Type': 'application/json',
      },
    });
  } catch(e) { return; };    
}

async function serviceEntries(sParam) {
  try {
    const response = await axios.get('https://worker-still-band-8abe.xsbrt.workers.dev/', {
      params: {
        s: sParam
      }
    });
    return response.data;
  } catch (error) {
    return null;
  };
  return null;
}

async function getLatest() {
  try {
    const response = await axios.get('https://worker-misty-night-e212.xsbrt.workers.dev/');
    return response.data;
  } catch (error) {
    return null;
  };
}

async function copyClipboard(txt) {
    const res = await navigator.clipboard.writeText(txt);
    alert('Use CTRL + V to paste this result from your clipboard');
  }

  async function shareSometing(it) {                
      if (navigator.share) {
        navigator.share(it);
        saveEntity(it);
      };
    }

  function randomString() { return Math.random().toString(36).substring(2, 6) + Math.random().toString(36).substring(2, 6) }

  async function saveEntity(it) {
    try { 
        it.mod = new Date().toISOString();
        it = JSON.parse(JSON.stringify(it));
        addLocalStorageDataByName(it.name,it);
    } catch(error) {
        return;
    };   
  }

  async function deleteItem(it) {
    try {
        await alasql("DELETE FROM notes WHERE id = '" + it.id + "'");
    } catch(error) { return };    
  }

  function toHumanReadableDate(isoString) {
    const date = new Date(isoString);
    const year = date.getFullYear();
    const month = String(date.getMonth() + 1).padStart(2, '0'); // Months are 0-based
    const day = String(date.getDate()).padStart(2, '0');
    const hours = String(date.getHours()).padStart(2, '0');
    const minutes = String(date.getMinutes()).padStart(2, '0');
    return `${year}-${month}-${day} ${hours}:${minutes}`;
  }

  async function downloadData(res) {    
    if (res&&res.length > 0) {
        const filename = randomString() + ".xlsx"
        await alasql('SELECT * INTO XLSX("' + filename + '",{headers:true}) FROM ?', [res]);
        res = null;
    };            
  }

  function convertMultipleSpacesToSingle(str,nonalpha) {
    if (nonalpha) { str = str.replace(/[^a-zA-Z0-9\s]/g, '') };
    return str.replace(/\s+/g, ' ').trim();  
  }

  function getRandomItems(arr, count) {
    // Clone the array to avoid modifying the original array
    let tempArr = arr.slice();    
    // Ensure count is not greater than the array length
    count = Math.min(count, arr.length);
    // Fisher-Yates Shuffle algorithm to randomize the array
    for (let i = tempArr.length - 1; i > 0; i--) {
        let j = Math.floor(Math.random() * (i + 1));
        [tempArr[i], tempArr[j]] = [tempArr[j], tempArr[i]];
    }
    // Return the first 'count' items from the shuffled array
    return tempArr.slice(0, count);
}

function getMasterPrompt(user_input) {
  let array = getAreas();
  array = alasql("SELECT title, role FROM ?",[array]);
  return [
    { 
      role: "system", 
      content: `Given the user input "${user_input}", perform the following tasks:\n\n1. Summarize the issue based on the input.\n2. Review the provided areas of expertise from the JSON array (${array}) and select the ones most relevant to the user's input. Not all items need to be selected, only those that best match the input.\n3. For each selected area, provide a brief summary and generate a specific ChatGPT prompt that could be used to create a response for that area, adhering to the "role" and "action" specifications provided in the JSON.\n\nReturn the results in the following JSON format:\n\n{\n  "issueSummary": "A brief summary of the user's issue.",\n  "matchedAreas": [\n    {\n      "areaTitle": "The title of the matched area",\n      "areaSummary": "A brief summary of how this area relates to the user's issue.",\n      "chatGptPrompt": "The specific ChatGPT prompt generated for this area."\n    },\n    ...\n  ],\n  "c": "Identified LCC code"\n}`
    },
    { 
      role: "user", 
      content: `Also, identify the most accurate Library of Congress Classification (LCC) code that corresponds to the provided user input, returning the identified LCC code in a simple format, do not include any additional information or context in your response, just the LCC code itself, assigning it to the final JSON as 'c'. The user has asked about the following: "${user_input}".` 
    }
  ];
}
  function getAreas() {
    return [
        {
          "s": 1,
          "title": "BenefitsGPS",
          "shortTitle": "BenefitsGPS",
          "icon": "mdi-crosshairs-gps",
          "subtitle": "Navigate Social Services with BenefitsGPS",
          "url": "https://xsbrt.com/benefitsGPS.html",
          "description": "Simplify your journey through social services with BenefitsGPS. Discover the path to the resources you need.",
          "label": "Tell Us Your Social Service Needs",
          "hint": "Narrate your challenges; we'll guide you to the right support.",
          "placeholder": "What social service support do you need? Share your challenges.",
          "role": "You are a highly knowledgeable social services case worker assistant with extensive experience in social work, community development, and economic frameworks. You have a deep understanding of the Urban Institutes Upward Mobility Framework and various local and federal resources available to support underserved populations. Your expertise also includes explaining complex concepts in a simple and accessible manner, suitable for an 8th-grade audience. A user is seeking assistance to connect with resources and programs that can support their community, particularly focusing on underserved populations. Using your knowledge up to April 2023, provide a comprehensive guide that uses the Upward Mobility Framework, tailored to their geographic location. Highlight relevant organizations, nonprofits, NGOs, and local/federal programs within or accessible from their location that align with the framework and can significantly benefit the user's community. Ensure that your response is structured as a JSON array containing 16 objects, each representing a different resource. Each object should include the following fields: 'name', 'phone', 'website', and 'directions'. The 'directions' field should provide clear, concise, and actionable guidance on how to engage with the resource, tailored to an 8th-grade audience and the user's geographic location. Remember, the goal is to empower the user and their community by connecting them with the most relevant and impactful resources, fostering a supportive environment for underserved populations, and promoting sustainable and inclusive development.",
          "action": "Provide a guide for connecting underserved communities with supportive resources, using the Upward Mobility Framework. If a geographic location is mentioned, tailor the guide to that area; otherwise, select general resources that are widely applicable. You will never make any information up and use no hypotheticals or hallucinations. Generate a JSON object with the key 'result' containing an array of 16 resources, including organizations, nonprofits, NGOs, and programs, both local and federal, that can aid the community. For each resource, include:'name': The name of the resource.'phone': Contact phone number.'website': URL for more information.'directions': Step-by-step guidance on how to engage, written for an 8th-grade reading level. If a location is provided, make the directions specific to that area; otherwise, provide general advice.Ensure the information is up-to-date as of April 2023 and clearly outlines how each resource can support the community, especially focusing on underserved groups.",
          "examples": [
            "Seeking guidance on accessing specialized support services for children with autism within the metropolitan area, focusing on educational and therapeutic resources.",
            "Navigating the process for emergency housing assistance after a natural disaster, with a need for immediate shelter solutions and long-term recovery support.",
            "Exploring opportunities for vocational training and apprenticeship programs in the renewable energy sector, aimed at underemployed adults looking to transition careers.",
            "Identifying mental health and counseling services for veterans dealing with PTSD, with an emphasis on programs offering family support and community integration.",
            "Looking for comprehensive nutritional assistance programs for low-income families, including food banks, SNAP benefits, and healthy eating education.",
            "Assistance required for elderly immigrants seeking citizenship and language services, focusing on legal aid and ESL classes that accommodate seniors.",
            "Urgent need for financial aid resources to cover medical expenses for a chronic illness, including charity programs and government assistance options.",
            "Exploring secondary education completion programs for adults, specifically those offering night classes or online learning options to accommodate working schedules.",
            "Seeking affordable and accessible dental care services for uninsured individuals, with a focus on clinics offering sliding scale fees based on income.",
            "Assistance with enrolling in utility discount programs for families facing financial hardship, including electricity, heating, and water bill support.",
            "Inquiring about support groups and therapy services for adolescents struggling with mental health issues, especially services that engage through art and music therapy.",
            "Seeking employment reentry programs for formerly incarcerated individuals, focusing on job training, placement services, and legal assistance for record expungement."
          ]
        },
        {
          "s": 2,
          "title": "FundMagnet",
          "shortTitle": "FundMagnet",
          "icon": "mdi-magnet",
          "subtitle": "Boost Your Fundraising with FundMagnet",
          "url": "https://xsbrt.com/fundmagnet.html",
          "description": "Supercharge your fundraising efforts with FundMagnet's innovative AI solutions. Elevate your mission today.",
          "label": "Share Your Fundraising Mission",
          "hint": "Inspire with your story; let's unlock funding opportunities.",
          "placeholder": "What's your fundraising dream? Describe your mission.",
          "role": "You are a professional fundraiser, grant researcher and grant writer with over 30 years of experience successfully securing funding for many different nonprofits from foundations, corporations and charitable trusts. You only return valid, well-formed JSON based on data up to April 2023. No hallucinations, hypotheticals or made-up information. You will never make any information up and use no hypotheticals or hallucinations. You'll be 100% honest in your answers and not make up any facts or information.",
          "action": "Generate a JSON object with the key 'result' containing an array of 15 diverse, unique, and interesting funding entities. Ensure that no more than half of the entries are major funders and prioritize smaller funders in the ranking. The array should be ordered from best to worst matches based on alignment with the user provided information. Each entity must have a name, phone (formatted as 202-123-4567; never made up), website (never made up), and summary. All entries in the 'result' array must be unique and duplicate-free.You will never make any information up and use no hypotheticals or hallucinations. You never use made-up information and only show valid, real funders. You'll be 100% honest in your answers and not make up any facts, phone numbers, websites, funding organizations or information. Only entries which provide funding.",
          "examples":[
            "Launching a grassroots campaign to equip inner-city schools with state-of-the-art STEM labs, aiming to bridge the educational divide.",
            "Expanding a clean water initiative into rural communities in sub-Saharan Africa, incorporating solar-powered water purification technology.",
            "Developing a mobile app to connect food surplus from restaurants with food banks, addressing urban food wastage and hunger simultaneously.",
            "Funding a series of workshops aimed at empowering women with digital skills in Southeast Asia, fostering economic independence through technology.",
            "Establishing a network of mental health first responders in communities affected by natural disasters, offering immediate support and long-term care.",
            "Creating an incubator for sustainable agriculture startups in South America, focusing on innovations in crop resilience and water conservation.",
            "Supporting the preservation of indigenous languages through a digital archive, involving community-led documentation and accessible educational resources.",
            "Implementing a coral reef restoration project using 3D-printed structures to promote marine biodiversity in endangered reef ecosystems.",
            "Launching a fellowship program for young social entrepreneurs focused on climate change solutions, providing mentorship, funding, and global exposure.",
            "Building an online platform for virtual volunteerism, connecting skilled volunteers with NGOs around the world for remote support and collaboration.",
            "Funding research into biodegradable packaging alternatives, aiming to partner with major food and beverage companies for adoption and scale.",
            "Establishing a chain of affordable, solar-powered charging stations for electric vehicles in underserved urban neighborhoods, promoting cleaner transportation options.",
            "Organizing a global hackathon to develop open-source software solutions for non-profits, enhancing operational efficiency and service delivery through technology.",
            "Developing a community-based program to reintroduce endangered species into their natural habitats, involving local schools and volunteers in conservation efforts.",
            "Creating a social impact bond to fund home insulation projects in low-income communities, reducing energy costs and emissions through improved housing."
          ]         
        },
        {
          "s": 3,
          "title": "Decision Support",
          "shortTitle": "Decisions",
          "icon": "mdi-comment-question-outline",
          "subtitle": "Clarify Your Choices with Decision Support",
          "url": "https://xsbrt.com/decisions.html",
          "description": "Cut through decision dilemmas with xsbrt. Enhance your decision-making prowess.",
          "label": "Detail Your Decision-Making Dilemmas",
          "hint": "Explain the choices you're facing; we'll provide clarity.",
          "placeholder": "What decisions are you struggling with? Share for tailored advice.",
          "role": "You are a decision-making expert with a focus on practical application of strategies. Given the strategies: Uphill Decisions, The Two-Minute Rule, Solomon's Paradox, The Hesitation Strategy, and Regret Minimization, provide a direct and concise answer to the posed question. Apply the most suitable strategy based on the context, ensuring your response is well-structured, detailed, and formatted as a JSON object.",
          "action": "Review the users input and choose the absolute best decision strategies in order from most to least and return as a JSON object in the format of: {Name of strategy: \"response\", etc.}. If a strategy does not match, then leave it out of the results.",
          "examples":[
            "Analyze the trade-offs of accepting a high-profile but demanding job versus a lower-stress position with more work-life balance.",
            "Assess the benefits and risks of transitioning from a traditional brick-and-mortar business model to an entirely online operation.",
            "Determine the viability of expanding business operations internationally in the face of current global economic uncertainties.",
            "Evaluate the potential impact of returning to school for an advanced degree against the backdrop of evolving industry demands.",
            "Debate the merits of investing in renewable energy projects given current market volatility and long-term environmental goals.",
            "Consider the implications of adopting a remote-first policy for your workforce, balancing employee satisfaction and collaboration challenges.",
            "Decide on the strategic value of entering a partnership with a competitor to explore new market opportunities versus going solo.",
            "Weigh the pros and cons of implementing a significant technology overhaul in your organization against the backdrop of digital transformation trends.",
            "Contemplate the decision to pivot your business focus in response to changing consumer behaviors driven by recent global events.",
            "Judge the appropriateness of launching an aggressive marketing campaign in a sensitive economic climate to gain market share."
          ]
        },
        {
          "s": 4,
          "title": "Media Management",
          "shortTitle": "Media",
          "icon": "mdi-bullhorn",
          "subtitle": "Elevate Your Media Presence with Strategic Insights",
          "url": "https://xsbrt.com/media.html",
          "description": "Dominate the media landscape with our expert guidance. Craft and control your narrative like never before.",
          "label": "Define Your Media Objectives",
          "hint": "Outline your media ambitions; we'll strategize for impact.",
          "placeholder": "What are your media goals or challenges? Share for expert strategies.",
          "role": "As a Media Reputation Management Specialist, I have extensive experience in managing stories in the media and helping organizations protect and enhance their reputation. I am skilled in analyzing media coverage, developing strategic communication plans, and implementing effective management strategies. My expertise lies in identifying key issues, crafting compelling narratives, and engaging with media outlets to mitigate negative publicity and restore public trust. I am also an expert at identifying at the exact media outlets, including social media, as well as the deliverables those media outlets will respond too.",
          "action": "Generate a strategic communication plan as a single JSON object based on what the user has asked about. The JSON should include 'media_analysis': a detailed, yet concise one paragraph analysis of media related to the issue, 'key_narrative_points': array of key narrative points, 'deliverables': array of the best matched, most effective targeted media deliverables: [{'deliverableDescription','chatGPT_prompt'}]. Also, provide a 'rapid_response_strategy': a detailed, yet concise one paragraph rapid response strategy and 'monitoring_and_adjustment_steps': a detailed, yet concise one paragraph for reputation management. All entries in must be unique and duplicate-free. No hypotheticals, hallucinations or made-up information.",
          "examples": [
            "Launch a multi-platform content series to highlight a tech company's commitment to data privacy, including expert interviews and customer testimonials.",
            "Orchestrate an influencer partnership campaign for a beauty brand aiming to break stereotypes, focusing on diversity and inclusivity.",
            "Implement a crisis communication strategy for a food and beverage company addressing supply chain issues, with a focus on transparency and corrective actions.",
            "Design a digital PR campaign for an eco-friendly apparel brand launching a recycled materials line, leveraging social media and sustainability influencers.",
            "Coordinate a press event for a medical research firm announcing a breakthrough in cancer treatment, targeting health and science journalists.",
            "Execute a reputation repair program for a hospitality business post-crisis, using customer reviews and community engagement to rebuild trust.",
            "Develop a social media advocacy campaign for a non-profit organization focusing on mental health awareness, incorporating user-generated content and expert advice.",
            "Launch a video marketing campaign for an educational tech startup showcasing user success stories and platform features, aimed at parents and educators.",
            "Create an interactive online forum for a political candidate to address constituents' concerns directly, enhancing public engagement and feedback.",
            "Initiate a cross-media storytelling project for a museum's new exhibit, combining AR experiences, social media, and traditional media coverage."
          ]
        },
        {
          "s": 5,
          "title": "xsbrt",
          "shortTitle": "xsbrt",
          "icon": "mdi-lightbulb-on-outline",
          "subtitle": "Leverage Expertise on Demand with xsbrt",
          "url": "https://xsbrt.com/xsbrt.html",
          "description": "Unlock tailored AI-driven solutions with xsbrt, offering expert insights to navigate your unique challenges effectively. Describe your issue, and our AI specialists will provide customized analyses and actionable strategies, all aimed at empowering your decision-making with precision and clarity.",
          "label": "Specify Your Insight Needs",
          "hint": "What challenge needs solving? Describe for expert AI assistance.",
          "placeholder": "What insights or solutions are you seeking? Detail your needs.",
          "role": "You are a ChatGPT specialist focused on creating concise, structured outputs for specific user inquiries.",
          "action": "Based upon the user input, choose the best available 'expert' with the title, and create at least three 'deliverables' each with a 'name', 'description', and 'prompt' to create the deliverable. Each deliverable should aim for a minimum of 900 words. Additionally, create a 'summary' of the insights provided. Return the output in the following format: {\"expert\": \"Title of the expert\", \"deliverables\": [{\"name\": \"In-Depth Analysis of [Specific Topic]\", \"description\": \"A comprehensive analysis of a specific aspect of [Topic]\", \"prompt\": \"As a [title of the 'expert'], provide a detailed analysis of [Specific Aspect].\"}], \"summary\": \"Provide a concise summary that clearly outlines the user's issue, the expert's role and how the deliverables address it. Keep the summary under 100 words.\"}",
          "examples":[
            "Develop an AI-driven predictive maintenance system for reducing downtime in manufacturing operations, including an analysis of implementation costs and ROI.",
            "Explore the integration of virtual reality into online retail to enhance customer experience, detailing the technical requirements and potential market impact.",
            "Assess the feasibility and security implications of adopting cryptocurrency payments for an online marketplace, with a focus on consumer adoption barriers.",
            "Design an employee wellness program leveraging wearable technology to monitor and improve health outcomes, including a cost-benefit analysis.",
            "Evaluate the use of drone technology in agricultural surveillance to enhance crop yield predictions, considering environmental and regulatory challenges.",
            "Create a strategy for leveraging big data analytics in personalized marketing campaigns, assessing data privacy concerns and customer engagement metrics.",
            "Investigate the application of machine learning algorithms in financial fraud detection, outlining the integration process with existing banking systems.",
            "Propose a mobile app solution for improving urban public transportation efficiency, including user adoption strategies and potential city partnerships.",
            "Assess the impact of implementing a flexible work arrangement policy on productivity and employee satisfaction in a corporate setting.",
            "Analyze the benefits and challenges of introducing a subscription-based model for a traditional retail business, including customer retention strategies.",
            "Develop a plan for a startup to enter the competitive fitness technology market, focusing on unique value propositions and market differentiation strategies.",
            "Evaluate the implications of new data protection regulations on a multinational corporation’s data management practices, including compliance solutions."
          ]
        },
        {
          "s": 6,
          "title": "Future",
          "shortTitle": "Future",
          "icon": "mdi-sitemap",
          "subtitle": "Forecast Your Future with Expert Insights",
          "url": "https://xsbrt.com/future.html",
          "description": "Navigate towards a promising future with our analytical predictions. Harness the power of foresight.",
          "label": "Inquire About Future Trends",
          "hint": "Curious about the future? Share your questions for profound forecasts.",
          "placeholder": "What future trends are you interested in? Inquire for expert predictions.",
          "role": "Your role is that of a highly accurate futurist, specialized in making precise and well-researched forecasts about future trends and events. In light of the historical challenges in predicting the future, including our tendency to project current technologies and societal norms onto future scenarios, and the nonlinear nature of progress, your task is to develop more effective methods for forecasting future developments. This involves considering lessons learned from past predictions, both accurate and inaccurate, and integrating insights from current technological and scientific knowledge, an understanding of human nature, and conceptual, broad-spectrum thinking.",
          "action": "Given the historical challenges in accurately predicting the future, including our tendency to apply current technologies and societal norms to future scenarios, and the nonlinear nature of progress, how can we more effectively forecast future developments using the information provided by the user below? Consider the lessons learned from past predictions, both accurate and inaccurate, and integrate insights from current technological and scientific knowledge, an understanding of human nature, and conceptual, broad-spectrum thinking. Focus on envisioning positive outcomes that drive us toward a future where we strive for equality, freedom, health, and safety, rather than attempting to outline specific technologies or societal structures. How can we use this approach to construct a more hopeful and realistically attainable vision of the future? Your response will strictly follow this JSON format: {\"forecasting_approach\": {\"understanding_past_lessons\": \"response focused on user input\", \"current_knowledge_integration\": \"response focused on user input\", \"human_nature_understanding\": \"response focused on user input\", \"broad_spectrum_thinking\": \"response focused on user input\", \"positive_outcome_focus\": \"focused on user input\", \"continuous_knowledge_update\": \"response focused on user input\"}, \"forecast\": {\"short_term\": \"response focused on user input\", \"medium_term\": \"response focused on user input\", \"long_term\": \"response focused on user input\"}}",
          "examples": [
            "Investigate how quantum computing could revolutionize data encryption and cybersecurity measures by 2035.",
            "Assess the trajectory of lab-grown meat production and its potential to become a mainstream food source by 2040, considering environmental and ethical impacts.",
            "Forecast the integration of augmented reality in daily life and its implications for education, entertainment, and professional training by 2030.",
            "Analyze the evolution of smart cities and their ability to enhance sustainability, efficiency, and citizen well-being over the next two decades.",
            "Discuss the role of gene editing technologies, like CRISPR, in eradicating hereditary diseases by 2045, including ethical and societal considerations.",
            "Predict the development of global water management strategies to combat scarcity and ensure access to clean water worldwide by 2050.",
            "Evaluate the impact of autonomous drone delivery systems on urban logistics and retail businesses by 2030, including regulatory and safety challenges.",
            "Assess the future of wearable health technology and its potential to personalize healthcare and disease prevention by 2035.",
            "Explore the implications of deepfake technology on media, privacy, and democracy over the next 15 years, considering countermeasures and ethical debates.",
            "Forecast the advancements in battery technology and their effect on renewable energy storage solutions and electric vehicle adoption by 2040.",
            "Analyze the potential for vertical farming and urban agriculture to address food security and sustainability challenges by 2035.",
            "Discuss the integration of AI in governance and public administration, predicting its influence on policy-making and citizen engagement by 2040."
          ]
        },
        {
          "title": "Communicate",
          "shortTitle": "Communicate",
          "icon": "mdi-account-search",
          "subtitle": "Master Tailored Communication with Psychology-Driven Strategies",
          "url": "https://xsbrt.com/communicate.html",
          "description": "Transform your messaging with our psychology-based communication strategies for unmatched engagement.",
          "label": "Explore Your Communication Strategy",
          "hint": "What's your communication challenge? We'll craft the perfect strategy.",
          "placeholder": "What communication hurdles are you facing? Let's strategize.",
          "role": "You are a custom communications specialist with a behavioral psychology focus. Your objective is to apply the principles of the 'Big Five' personality traits and the Schwartz Value Circumplex to create tailored communications strategies based on end-user input. This role capitalizes on your expertise in both psychology and communications, requiring you to analyze and apply complex psychological concepts to create highly customized and effective communication strategies based on specific user needs.",
          "action": "Analyze and provide communications feedback on the user input based on your extensive knowledge. Your results will be returned as a well-formed JSON object in the following format: {\"analysis\": [\"text of analysis\"], \"custom_communications_development\": [\"text of development\"], \"scenario_based_application\": [\"text of scenario-based application\"], \"outcome_evaluation\": [\"text of outcome evaluation\"], \"reporting_and_advising\": [\"text of reporting and advising\"]}",
          "examples": 
          [
            "Develop a communication strategy for a team facing low morale and high stress.",
            "Create a tailored message to increase customer loyalty for an e-commerce brand.",
            "Advise on improving communication between diverse teams within a multinational corporation.",
            "Craft a strategy to communicate a major company restructuring in a way that minimizes employee anxiety.",
            "Design a communication plan to engage a community in a public health initiative.",
            "Formulate a messaging strategy for a non-profit seeking to increase donor engagement.",
            "Propose a communication approach to address conflicts arising from remote work challenges.",
            "Develop a persuasive communication strategy for a political campaign targeting young voters.",
            "Create a communication plan for a tech company launching a controversial product.",
            "Advise on communication techniques to enhance patient compliance in a healthcare setting."
          ]
        },
        {          
          "title": "Pros and Cons Analysis",
          "shortTitle": "Pros and Cons",
          "icon": "mdi-thumbs-up-down",
          "subtitle": "Make Informed Decisions with Our Pros and Cons Analysis",
          "url": "https://xsbrt.com/?s=mdi-thumbs-up-down",
          "description": "Balance your decisions with a clear overview of pros and cons. Empower your choices with our analytical tool.",
          "role": "You are an AI capable of generating a detailed list of pros and cons for any given topic. Your task involves making an educated guess about pros and cons, ranking them from most to least important, and assigning a numerical value from 0.0 to 1.0 to each, based on their relative importance.",
          "action": "Analyze the submitted topic and provide a structured list of pros and cons. Results will be returned as one array of JSON objects, each with a 'label' ('pro' or 'con'), a 'ranking', 'description' of item, and a 'numerical' importance value.",
          "label": "Begin Your Pro-Con Analysis",
          "hint": "Facing a tough choice? Detail it for a balanced evaluation.",
          "placeholder": "What decision are you pondering? Submit for a comprehensive analysis.",
          "examples": [
            "Analyze the pros and cons of implementing a four-day workweek for a tech company.",
            "Evaluate the benefits and drawbacks of pursuing a higher education degree in today's job market.",
            "Determine the advantages and disadvantages of remote work for productivity and work-life balance.",
            "Assess the positive and negative aspects of starting a business in the current economic climate.",
            "Examine the pros and cons of investing in cryptocurrency as a long-term investment strategy.",
            "Consider the benefits and risks of relocating to a new city for a job opportunity.",
            "Analyze the advantages and disadvantages of using social media for small business marketing.",
            "Evaluate the pros and cons of adopting electric vehicles for a company's sales fleet.",
            "Determine the positive and negative impacts of renewable energy adoption on the environment.",
            "Assess the benefits and drawbacks of freelance work versus full-time employment."
          ]
        },
        {
          "s": 9,
          "title": "Risk Assessment",
          "shortTitle": "Risk Assessment",
          "icon": "mdi-key-plus",
          "subtitle": "Strategize with Confidence Using Our Risk Assessment",
          "url": "https://xsbrt.com/?s=mdi-thumbs-up-down",
          "description": "Discover and mitigate risks with our expert analysis. Navigate decisions with strategic insights into potential outcomes.",
          "role": "The role of a Senior Risk Assessment Analyst involves a comprehensive and expert-level approach to evaluating risks associated with various scenarios, projects, or decisions. Here's a detailed description of this role:1. **Expertise in Risk Analysis**: A Senior Risk Assessment Analyst possesses deep knowledge and understanding of risk analysis methodologies. This includes the ability to identify, evaluate, and prioritize risks based on various factors like probability, impact, and severity.2. **Scenario Evaluation**: They are skilled in analyzing a wide range of scenarios, from business operations to project management and strategic initiatives. This involves understanding the nuances of each scenario to identify potential risks.3. **Risk Assessment Frameworks**: They are adept at using and possibly developing structured frameworks for risk assessment. This includes creating models and methodologies that can be applied to different scenarios for consistent risk evaluation.4. **Data Analysis and Interpretation**: This role requires strong analytical skills to interpret data related to risks. They analyze historical data, market trends, and other relevant information to inform their risk assessments.5. **Communication Skills**: Senior Risk Assessment Analysts must effectively communicate their findings to stakeholders. This involves preparing detailed reports and presentations that clearly convey the risks, their potential impacts, and mitigation strategies.6. **Decision-Making Support**: They provide crucial input to help decision-makers understand the risk landscape. Their assessments guide businesses in making informed decisions, balancing risk with potential opportunities.7. **Mitigation Strategy Development**: Beyond identifying risks, they are responsible for suggesting or developing strategies to mitigate identified risks. This could include recommending policy changes, process improvements, or other measures to manage or reduce risks.8. **Leadership and Mentorship**: As a senior role, it often involves leading a team of analysts, mentoring junior staff, and guiding them in complex risk assessments.9. **Regulatory Compliance and Industry Knowledge**: They must stay abreast of relevant regulations and industry standards, ensuring that risk assessments comply with legal and regulatory requirements.10. **Continual Learning and Adaptation**: Given the ever-changing nature of risks, especially with technological advancements and market changes, they are expected to continually update their skills and knowledge.In summary, a Senior Risk Assessment Analyst is an expert in identifying and evaluating risks, providing detailed assessments and mitigation strategies, and supporting decision-making processes in complex and dynamic environments.",
          "action": "Conduct an in-depth analysis of the provided scenario. Produce a structured and detailed risk assessment in JSON format. Your assessment should comprehensively cover the following aspects: 'Long_Term_Impact', 'Financial_Consequences', 'Personal_Development', and 'Social_Implications'. For each aspect, identify and enumerate specific risks, characterized by 'Risk_Factor', 'Impact_Level', and 'Mitigation_Strategies'. Your JSON output should be meticulously organized, ensuring clarity, logical structuring, and completeness. Apply your expertise to deliver an analysis that reflects deep insights, thoroughness, and professional acumen in risk assessment.",
          "label": "Outline Your Risk Evaluation Needs",      
          "hint": "What risks are you evaluating? Share for detailed assessment.",
          "placeholder": "Describe the scenario for an in-depth risk analysis.",
          "examples": [
            "Delve into the specific risks and opportunities of integrating AI into financial fraud detection systems, including false positive rates and customer trust implications.",
            "Assess the implications of adopting a four-day workweek on productivity and employee well-being within a tech company, considering industry benchmarks and cultural readiness.",
            "Examine the cybersecurity vulnerabilities of implementing a decentralized identity management system using blockchain, focusing on data privacy and regulatory compliance.",
            "Analyze the operational and environmental risks of launching a commercial space tourism venture, including safety protocols and potential impact on near-space ecosystems.",
            "Evaluate the financial sustainability and market acceptance risks of introducing an electric vehicle subscription model in urban centers.",
            "Investigate the legal and ethical risks of employing facial recognition technology in retail for personalized marketing, considering consumer privacy concerns and bias in algorithm design.",
            "Conduct a risk assessment on the shift towards telehealth services post-pandemic, focusing on patient data security and the digital divide in access to healthcare.",
            "Assess the impact of implementing AI-driven personalization in e-learning platforms on student privacy and educational equity.",
            "Evaluate the strategic risks and competitive advantages of entering the plant-based food industry, analyzing consumer trends and supply chain resilience.",
            "Analyze the potential risks and regulatory challenges of a biotech startup focusing on gene therapy for rare diseases, including clinical trial obstacles and market adoption rates.",
            "Investigate the sustainability and ethical sourcing risks in the fashion industry's shift towards circular economy models, including consumer acceptance and supply chain transparency.",
            "Conduct a detailed risk assessment of using drones for urban delivery services, focusing on air traffic regulations, safety concerns, and public perception."
          ]
        },
        {
          "s": 10,
          "title": "Lobbyist",
          "shortTitle": "Lobbyist",
          "icon": "mdi-account-switch",
          "subtitle": "Maximize Your Advocacy with Professional Lobbying Strategies",
          "url": "https://xsbrt.com/?s=mdi-account-switch",
          "description": "Elevate your cause with our seasoned lobbying expertise. Craft compelling advocacy strategies for impactful results.",
          "role": "Imagine you are a very experienced United States congressional lobbyist. You assist users in effectively lobbying the U.S. congress for their chosen issues by providing specialized, strategic, and impactful advice. Your task is to help users effectively lobby for their chosen issues. When a user provides an issue they are passionate about, you will assist them by creating highly effective and persuasive deliverables to support their lobbying efforts. Remember, your role is to provide clear, concise, and impactful advice, drawing on your expertise as a lobbyist. Your responses should be informative and tailored to the specific issue the user is interested in. Ensure that all advice and strategies adhere to ethical lobbying practices and encourage transparent, honest communication. All of your results are well formed JSON. No hallucinations or any made-up information.",
          "action": "1. 'Complexities': Start with a brief description of the issue's complexities. Then, create a ChatGPT 'prompt' for an in-depth complexities analysis. The output should be a JSON object with two fields: 'description' for the brief description, and 'prompt' for the ChatGPT prompt. 2. 'Stakeholders': List the stakeholders involved in a JSON array format. 3. 'Legislative_History': Provide a brief legislative history description and a ChatGPT prompt for a detailed legislative history analysis. Format the output as a JSON object with 'description' and 'prompt' fields. Next, develop a 'Lobbying_Strategy' as JSON objects: 1. 'Potential_Alliances': Suggest potential alliances in a JSON array. . 'Influence_Public_Opinion': Include a brief description of how to influence public opinion, along with a ChatGPT prompt for a deeper analysis. Output this as a JSON object with 'description' and 'prompt' fields. Provide 'Communication_Guidance', structured in JSON: 1. 'Tone': Advise on the appropriate tone in a simple JSON field. 2. 'Content': Guide on the content strategy in a JSON field.",
          "label": "Craft Your Advocacy Approach",
          "hint": "What's your lobbying objective? We provide the strategy.",
          "placeholder": "What issue are you advocating for? Share for strategic guidance.",
          "examples": [
            "Strategize a campaign for amending the Clean Air Act to include stricter emissions standards for heavy industries, focusing on health impact studies and regional air quality data.",
            "Lobby for the passage of a bill providing tax incentives for small businesses adopting sustainable practices, leveraging economic impact analyses and case studies of successful green initiatives.",
            "Coordinate advocacy efforts for a digital privacy act that protects consumer data from unauthorized collection and use, citing recent breaches and public concern over data security.",
            "Develop a lobbying strategy to secure increased federal investment in public transportation infrastructure, emphasizing environmental benefits and urban mobility improvements.",
            "Advocate for legislation that mandates mental health education in schools, supporting the initiative with statistics on youth mental health crises and the effectiveness of early intervention.",
            "Promote a bill that accelerates the transition to renewable energy by offering subsidies for solar and wind energy projects, using comparative studies on job creation and economic growth.",
            "Organize a lobbying campaign against the rollback of endangered species protections, highlighting the ecological impact and mobilizing public support through awareness campaigns.",
            "Champion a comprehensive drug reform policy aimed at decriminalizing possession and investing in addiction treatment programs, presenting comparative analysis from countries with successful models.",
            "Lobby for the introduction of a federal paid family leave act, drawing on comparative labor policies and their impact on workforce participation rates and economic stability.",
            "Advocate for increased funding and support for veteran reintegration programs, using data on veteran unemployment and homelessness to highlight the need for comprehensive services.",
            "Promote a legislative framework for ethical AI development and usage, focusing on potential risks to privacy, employment, and bias, backed by expert testimonies and international guidelines.",
            "Lobby for a national initiative to improve broadband access in rural areas, emphasizing the digital divide's impact on education, healthcare, and economic opportunity in underserved communities."
          ]
        },
        {
          "s": 11,
          "title": "Venture Capitalist",
          "shortTitle": "Venture Capitalist",
          "icon": "mdi-finance",
          "subtitle": "Strategic Investment Insights for High-Growth Opportunities",
          "url": "https://xsbrt.com/?s=mdi-finance",
          "description": "Unlock the potential of emerging markets and innovative startups with expert investment strategies. Leverage our experience to optimize your portfolio for maximum growth.",
          "role": "As a seasoned venture capitalist, your role is to guide users through the complex landscape of investments in startups and emerging markets. You provide strategic advice on identifying promising investment opportunities, evaluating startup potential, and managing investment portfolios. When users present an investment scenario, you assist by offering insights into market trends, startup viability, and portfolio strategies. Your advice is grounded in data-driven analysis, focusing on maximizing returns and mitigating risks. Responses should be precise, actionable, and tailored to the specific investment focus, ensuring compliance with regulatory standards and emphasizing data security.",
          "action": "Generate strategic investment insights based on the user's input, providing results in a JSON object with specific keys for analysis and strategy development. Each key includes a 'description' write a detailed, yet concise, summarizing of the analysis or strategy focusing on the user input, and a 'prompt' for creating a more detailed item:  \'Market_Insight\': {    \'description\': \'Analyzes the specified market's conditions, trends, and potential growth areas, delivering insights relevant to the user input.\',    \'prompt\': \'Given the user's input, perform a detailed market analysis focusing on current trends, growth opportunities, and key sectors for investment.\'  },      \'Investment_Strategy\': {    \'description\': \'Develops investment strategies aligned with the user's input, including portfolio diversification and risk management, to optimize returns.\',    \'prompt\': \'Create a comprehensive investment strategy for focused on the user input, incorporating portfolio diversification, risk assessment, and exit strategies to maximize investment success.\'  },    \'Scenario_Analysis\': {    \'description\': \'Outlines potential investment scenarios based on the user's input, analyzing impacts of market changes and suggesting navigation strategies.\',    \'prompt\': \'Develop scenario analyses for [User's Portfolio], exploring how various market conditions could affect investment outcomes and proposing strategic responses.\'  },    \'Strategic_Recommendations\': {    \'description\': \'Summarizes actionable recommendations based on the analyses, guiding the user's input on next steps for their investment strategy.\',    \'prompt\': \'Synthesize insights from the market and startup analyses, scenario planning, and investment strategy development into actionable recommendations for [User's Name], focusing on immediate and long-term investment actions.\'  }",
          "label": "Optimize Your Investment Strategy",
          "hint": "What's your investment focus? We'll help you navigate.",
          "placeholder": "Enter a market or startup for in-depth analysis and strategy.",
          "examples": [
            "Delve into the investment prospects of AI platforms specializing in non-invasive, real-time blood glucose monitoring for diabetes management, highlighting patent landscapes and regulatory pathways.",
            "Examine the potential for venture capital entry into the Indonesian digital wallet ecosystem, focusing on competitive differentiation strategies amidst Gojek and Grab's dominance.",
            "Assess the market readiness and innovation trajectory of biodegradable packaging startups targeting the quick-service restaurant sector in North America, considering consumer behavior shifts post-COVID-19.",
            "Analyze the strategic positioning and expansion feasibility of electric bicycle rental services in Scandinavian cities, with an emphasis on sustainability commitments and urban infrastructure compatibility.",
            "Investigate the investment appeal of SaaS solutions for cybersecurity in the hospitality industry, evaluating market needs versus current offerings and the impact of recent data breaches.",
            "Critique the business models and market penetration strategies of telemedicine services tailored for mental health support in rural communities of the Midwest, taking into account state-specific telehealth regulations.",
            "Scope the investment landscape for companies producing lab-grown meat, focusing on technological innovations, consumer acceptance, and regulatory hurdles in the European Union.",
            "Evaluate the disruptiveness of blockchain technology for enhancing transparency and reducing costs in cross-border payments for South Asian migrant workers, considering remittance volume trends and blockchain adoption barriers.",
            "Review the scalability and pedagogical outcomes of AR and VR applications in immersive learning for secondary education, analyzing pilot programs and partnership opportunities with school districts.",
            "Investigate the feasibility and market demand for large-scale solar energy projects in the Sahara Desert, assessing political stability, infrastructure challenges, and European energy import potential.",
            "Examine the investment potential in startups developing universal remote control devices for the burgeoning smart home market, focusing on AI integration for predictive household management.",
            "Critically assess the long-term viability and ethical considerations of investing in startups focused on CRISPR technology for the treatment of Huntington's Disease, considering clinical trial progress and patient advocacy group positions."
          ]
        }
      ]      
  }

  function setCookie(name, value, days) {
    let expires = '';
    if (days) {
      const date = new Date();
      date.setTime(date.getTime() + (days * 24 * 60 * 60 * 1000));
      expires = '; expires=' + date.toUTCString();
    }
    document.cookie = name + '=' + (value || '') + expires + '; path=/';
  }
  
  function getCookie(name) {
    const nameEQ = name + '=';
    const ca = document.cookie.split(';');
    for(let i=0; i < ca.length; i++) {
      let c = ca[i];
      while (c.charAt(0)==' ') c = c.substring(1,c.length);
      if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length,c.length);
    }
    return null;
  }
  
  function openIndexedDB() {
    return new Promise((resolve, reject) => {
        const request = indexedDB.open('MyDatabase', 1);

        request.onupgradeneeded = (event) => {
            const db = event.target.result;
            if (!db.objectStoreNames.contains('deviceData')) {
                db.createObjectStore('deviceData');
            }
        };

        request.onsuccess = (event) => {
            resolve(event.target.result);
        };

        request.onerror = (event) => {
            reject('Database error: ' + event.target.errorCode);
        };
    });
}

async function setIndexedDB(key, value) {
  return new Promise(async (resolve, reject) => {
      try {
          const db = await openIndexedDB();
          const transaction = db.transaction(['deviceData'], 'readwrite');
          const objectStore = transaction.objectStore('deviceData');
          const request = objectStore.put(value, key);

          request.onsuccess = () => {
              resolve();
          };

          request.onerror = (event) => {
              reject('Error writing to IndexedDB: ' + event.target.error);
          };
      } catch (e) {
          reject('Error opening IndexedDB: ' + e.message);
      }
  });
}

async function getIndexedDB(key) {
    const db = await openIndexedDB();
    const transaction = db.transaction(['deviceData'], 'readonly');
    const objectStore = transaction.objectStore('deviceData');
    return new Promise((resolve, reject) => {
        const request = objectStore.get(key);
        request.onsuccess = () => resolve(request.result);
        request.onerror = () => reject('Failed to retrieve from IndexedDB');
    });
}

  async function handleDeviceLocationAndId() {
     try {
        // Retrieve or generate an identifier
        let deviceId = getCookie('deviceId') || localStorage.getItem('deviceId') || await getIndexedDB('deviceId');
        if (!deviceId) {
            const fp = await fpPromise;
            const fingerprintResult = await fp.get();
            deviceId = fingerprintResult.visitorId;
            setCookie('deviceId', deviceId, 7); // 7 days expiration
            localStorage.setItem('deviceId', deviceId);
            await setIndexedDB('deviceId', deviceId);
        }
        return deviceId;
    } catch (e) {
        return;
    }; 
}

async function getLocationNew() {
    try {
      // Attempt to retrieve the device's location
      const location = await new Promise((resolve, reject) => {
          navigator.geolocation.getCurrentPosition(position => {
              resolve({
                  latitude: position.coords.latitude,
                  longitude: position.coords.longitude
              });
          }, error => {
              reject('Location access denied');
          });
      });
      // Store location data
      setCookie('deviceLocation', JSON.stringify(location), 7);
      localStorage.setItem('deviceLocation', JSON.stringify(location));
      await setIndexedDB('deviceLocation', JSON.stringify(location));
      return location;
  } catch (e) {
      return null;
  }
}

async function getCurrentLocation() {
  try {
      const location = await new Promise((resolve, reject) => {
          navigator.geolocation.getCurrentPosition(
              position => {
                  resolve({
                      latitude: position.coords.latitude,
                      longitude: position.coords.longitude
                  });
              }, 
              () => {
                  reject('Location access denied');
              }
          );
      });
      console.log(location); // Process the location object as needed
      return location;
  } catch (error) {
      console.error(error);
      // Handle the error or display a message to the user
  }
}

