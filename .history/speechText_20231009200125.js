<const SpeechToTextComponent = {
    >template: `
    <v-btn v-if="!isListening"
            color="primary" 
            class="my-4"
            title="Speak what you want"
            size="large"
            icon="mdi-account-voice"
            variant="tonal"
            ripple
            @click="startSpeechRecognition"
      > 
      </v-btn>
      <v-btn v-if="isListening"
          title="Stop talking" 
          variant="tonal" 
          ripple 
          color="error" 
          icon="mdi-voice-off" 
          @click="stopSpeechRecognition"
      ></v-btn>
    `,
    data() {
      return {
        text: '',
        artyom: null,
        isSupported: false,
        isListening: false
      };
    },
    async mounted() {
        this.artyom = new Artyom();
        this.isSupported = await this.artyom.speechSupported();
        console.log(this.isSupported);
    },
    methods: {
        async startSpeechRecognition() { 
            try {
                this.text = '';
                await this.artyom.initialize({ debug: false, soundex: true, continuous: true, listen: true, speed: 1 });       
                this.artyom.addCommands({
                    indexes: [/.*/],
                    smart: true,
                    action: (i, wildcard) => {
                        this.text = this.text + ' ' + wildcard.trim();
                    }
                });
                this.isListening = true;
            } catch (error) {
                console.error("Error starting speech recognition:", error);
            }               
        },

        async stopSpeechRecognition() { 
            try {
                this.isListening = false;
                this.artyom.fatality();
                this.$emit('speech-recognized', this.text.trim());
            } catch (error) {
                console.error("Error stopping speech recognition:", error);
            }               
        }
    }
};